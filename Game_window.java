import javax.swing.*;  
public class Game_window 
{  
    JFrame f;  
    Status game_status; // try not to use this - use this.controller.getGameStatus().whatevber
    private executable controller;

    private String inputValue = "";
    public String getinputValue() 
    {
        return inputValue;
    }

    public void setinputValue(String inputValue)
    {
        this.inputValue = inputValue;         
    }

    // constructors must be public to be useful
    public Game_window(executable controller) 
    {  
        this.controller = controller; // in case we need to "PHONE HOME" to our controller
        f=new JFrame();  // not sure why this is here
      // is there a better way to create a window?
        //String inputValue = JOptionPane.showInputDialog("Please input a value"); 
    }

    // call this somewhere - don't do it in the constructor
   public String askForInput()
    {
        String inputValue = JOptionPane.showInputDialog("Please input a value"); 
        return inputValue;
    }
}  