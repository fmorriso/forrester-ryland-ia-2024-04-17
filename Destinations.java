
public class Destinations
{
    //private int get_nearest_town;
    
    Status game_status;

    // NOTICE THE "PATTERN HERE" - repeat in the constructors of the other classes
    private executable controller;
    public Destinations(executable controller)
    {
        this.controller = controller;
    }
    
    private String destination = "";

    // YOU NEED TO FIGURE OUT WHEN/WHERE/WHY to call this method.
    public void someMethodThatSomebodyNeedsToCallFromSomewhere()
    {
            Status game_status = this.controller.getGameStatus();        
            switch (game_status.getdistanceTraveled())
            {
                case 0:
                    destination = "Omsk";
                    //return;   
                case 375:
                    destination = "Tyumen";
                    break;
                case 690:
                    destination = "Chelyabinsk";
                    break;
                case 1189:
                    destination = "Kazan";
                    break;
                case 1567:
                    destination = "Kyazan";
                    break;
                case 1692:
                    destination = "Moskow";
                    break;
            }        
        }

    public String get_nearest_town()
    {
        return destination;
    }

    /*public void set_nearest_town(String nearest_town)
    {
        this.get_nearest_town = game_status.getdistanceLeft();
    }*/
}